
function objToStrMap(obj) {
    let strMap = new Map();
    for (let k of Object.keys(obj)) {
      strMap.set(k, obj[k]);
    }
    return strMap;
  }
 function strMapToObj(strMap) {
    let obj = Object.create(null);
    for (let [k,v] of strMap) {
      obj[k] = v;
    }
    return obj;
  }
  function JsonToResultList(body) {
    let objectresult = new Map();
    let obj = Object.create(null);
    objectresult = objToStrMap(JSON.parse(body));
    obj = strMapToObj(objectresult)
    var result = {
      author: {},
      categories:[],
      items: []
    }
    if(obj.paging.total > 0){
      result = {
        author: {},
        categories: obj.filters.length == 0 ? '': Array.from(obj.filters[0].values[0].path_from_root, x => x.name),
        items: Array.from(obj.results, item => mapItems(undefined,item))
      }
    }
    
    return result;
  }
  function JsonToItem(description,body) {
    let objectresult = new Map();
    let obj = Object.create(null);
    objectresult = objToStrMap(JSON.parse(body));
    obj = strMapToObj(objectresult)
    var result = {
      author: {},
      items: []
    }
      result = {
        author: {},
        items: mapItems(description,obj)
      }

    
    return result;
  }
  function JsonToDescription(body) {
    let objectresult = new Map();
    let obj = Object.create(null);
    objectresult = objToStrMap(JSON.parse(body));
    obj = strMapToObj(objectresult)
    return obj.plain_text;
  }
  function mapItems(description, item){
    return itemsList = {
      id: item.id,
      title: item.title,
      price: {
        currency: item.currency_id,
        amount: item.price,
        decimals: 0
      },
      picture: item.pictures == undefined? item.thumbnail: item.pictures[0].url,
      condition: item.condition,
      free_shipping: item.shipping.free_shipping,
      state_name: item.address == undefined? '':item.address.state_name,
      sold_quantity: item.sold_quantity,
      description: description == undefined? '': description
    }
  }
  function validateErrors(jsonObj) {
    let objectresult = objToStrMap(JSON.parse(jsonObj));
    let obj = strMapToObj(objectresult)
    return result = {
      message: obj.message,
      error: obj.error,
      status: obj.status,
      cause: obj.cause 
    }
  }

module.exports = {
  objToStrMap,
  strMapToObj,
  JsonToResultList,
  JsonToItem,
  JsonToDescription,
  validateErrors
};