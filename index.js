const express = require('express');
const request = require("request");
const mapper = require("./mapper");
var cors = require('cors')

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }

const app = express();
app.use(express.json());


app.get("/Meli/api/items",cors(corsOptions),(req,res)=>{
    let validation;
    request("https://api.mercadolibre.com/sites/MLA/search?"+req._parsedUrl.query,(err,response,body)=>{
        validation = mapper.validateErrors(body);
        if(validation.status == undefined) {
            const result = mapper.JsonToResultList(body);  
            res.json(result)
        } else {
            res.json(validation);
        }
    })
});

app.get("/Meli/api/item/:id",cors(corsOptions),(req,res)=>{
    let result, description, validation;
    request("https://api.mercadolibre.com/items/"+req.params.id+"/description",(err,response,body)=>{
        validation = mapper.validateErrors(body);
        if(validation.status == undefined) {
            description = mapper.JsonToDescription(body);
            request("https://api.mercadolibre.com/items/"+req.params.id,(err,response,body)=>{
                result = mapper.JsonToItem(description,body);
                res.json(result);
            })
        } else {

            res.status(validation.status).send(validation.message)

        }       
    })
});



app.listen(8080, ()=>{
    console.log("Server listen on port 8080")
});